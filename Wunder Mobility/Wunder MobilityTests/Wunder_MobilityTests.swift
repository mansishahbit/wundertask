//
//  Wunder_MobilityTests.swift
//  Wunder MobilityTests
//
//  Created by Mansi Shah on 8/31/19.
//  Copyright © 2019 Mansi Shah. All rights reserved.
//

import XCTest
@testable import Wunder_Mobility

class Wunder_MobilityTests: XCTestCase {

    var apiRespository: APIRepository?
    
    override func setUp() {
        apiRespository = APIRepository()
    }

    override func tearDown() {
    }
    
    func testFetchCars() {
        let carsExpectation = expectation(description: "Cars")
        var carResponse: [Car]?
        
        apiRespository?.getCars { (cars, error) in
            carResponse = cars
            carsExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            XCTAssertNotNil(carResponse)
            XCTAssertNotNil(carResponse?.first?.carID)
        }
    }

    func testFetchCarDetails() {
        let carDetailsExpectation = expectation(description: "Car Details")
        var carResponse: CarDetails?
        
        apiRespository?.getCarDetails { (carDetails, error) in
            carResponse = carDetails
            carDetailsExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            XCTAssertNotNil(carResponse)
            XCTAssertNotNil(carResponse?.carID)
        }
    }
    
    func testRentCar() {
        let rentCarExpectation = expectation(description: "Rent Car")
        var carResponse: Bool?
        
        apiRespository?.rentCar { (result, error) in
            carResponse = result
            rentCarExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            XCTAssertNotNil(carResponse)
        }
    }

    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
