//
//  AppDelegate.swift
//  Wunder Mobility
//
//  Created by Mansi Shah on 8/31/19.
//  Copyright © 2019 Mansi Shah. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Configure: Main Application Window
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .white
        window?.makeKeyAndVisible()

        window?.rootViewController = UINavigationController(rootViewController: CarClusterVC())
        
        return true
    }

}

