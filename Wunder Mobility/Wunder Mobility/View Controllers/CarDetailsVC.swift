//
//  CarDetailsVC.swift
//  Wunder Mobility
//
//  Created by Mansi Shah on 9/1/19.
//  Copyright © 2019 Mansi Shah. All rights reserved.
//

import UIKit
import RxSwift
import Kingfisher

class CarDetailsVC: UIViewController {

    // MARK: Properties
    private final let disposeBag: DisposeBag
    private final let carID: Int
    private final let viewModel: CarDetailsVM?
    @IBOutlet weak var vehicleImage: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblClean: UILabel!
    @IBOutlet weak var lblDamaged: UILabel!
    @IBOutlet weak var lblLicensePlate: UILabel!
    @IBOutlet weak var lblFuel: UILabel!
    @IBOutlet weak var lblPricingTime: UILabel!
    @IBOutlet weak var lblPricingParking: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblZipcode: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblDamageDesc: UILabel!
    @IBOutlet weak var btnQuickRent: UIButton!
    
    // MARK: Init
    init(carID: Int, viewModel: CarDetailsVM?) {
        self.carID = carID
        self.viewModel = viewModel
        self.disposeBag = DisposeBag()
        super.init(nibName: R.nib.carDetailsVC.name, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Car Details"
        bindCarDetails()
        bindRentalResult()
    }
    
    // MARK: IBActions
    @IBAction func rentTapped(_ sender: UIButton) {
        viewModel?.rentCar()
    }
    
}

extension CarDetailsVC {
    private func bindCarDetails() {
        viewModel?.carDetails.subscribe(
            onNext: { [weak self] value in
                guard let `self` = self else { return }
                guard let carDetails = value else { return }
                self.fillData(carDetails: carDetails)
        }).disposed(by: disposeBag)
    }
    
    private func fillData(carDetails: CarDetails) {
        vehicleImage.kf.setImage(with: URL(string: carDetails.imageURLString))
        lblTitle.text = carDetails.title
        lblClean.text = carDetails.isClean ? "Yes" : "No"
        lblDamaged.text = carDetails.isDamaged ? "Yes" : "No"
        lblLicensePlate.text = carDetails.license
        lblFuel.text = "\(carDetails.fuel)"
        lblPricingTime.text = carDetails.pricingTime
        lblPricingParking.text = carDetails.pricingParking
        lblAddress.text = carDetails.address
        lblZipcode.text = carDetails.zip
        lblCity.text = carDetails.city
        lblDamageDesc.text = carDetails.damageDesc
    }
    
    private func bindRentalResult() {
        viewModel?.rentalResult.subscribe(onNext: { [weak self] value in
            guard let `self` = self else { return }
            guard let result = value else { return }
            self.showResult(withValue: result)
        }).disposed(by: disposeBag)
    }
    
    private func showResult(withValue: Bool) {
        let alertController = UIAlertController(
            title: "Car Renting " + (withValue ? "successfully done" : "failed"),
            message: nil,
            preferredStyle: .alert)
        self.present(
            alertController,
            animated: true,
            completion: { Timer.scheduledTimer(
                withTimeInterval: 3,
                repeats: false,
                block: { _ in
                    self.dismiss(animated: true, completion: nil)
            })})
    }
}
