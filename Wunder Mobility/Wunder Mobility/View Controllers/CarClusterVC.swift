//
//  CarClusterVC.swift
//  Wunder Mobility
//
//  Created by Mansi Shah on 9/2/19.
//  Copyright © 2019 Mansi Shah. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import RxSwift

class CarClusterVC: UIViewController {

    private final let locationManager: CLLocationManager
    private final let disposeBag: DisposeBag
    private final var viewModel: CarListVM
    private final let userAnnotation: MKPointAnnotation

    @IBOutlet weak var mapView: MKMapView!
    
    // MARK: Init
    init() {
        self.disposeBag = DisposeBag()
        self.userAnnotation = MKPointAnnotation()
        self.locationManager = CLLocationManager()
        self.viewModel = CarListVM()
        super.init(nibName: R.nib.carClusterVC.name, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Wunder Fleet"
        initiateLocationUpdates()
        initiateUserMarker()
        bindCars()
    }
    
}

extension CarClusterVC {
    
    private func initiateLocationUpdates() {
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        mapView.delegate = self
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        if let coor = mapView.userLocation.location?.coordinate {
            mapView.setCenter(coor, animated: true)
        }
    }
    
    private func initiateUserMarker() {
        userAnnotation.title = "Wunder User"
        userAnnotation.subtitle = "Current location"
        mapView.addAnnotation(userAnnotation)
    }
}

extension CarClusterVC {
    private func bindCars() {
        viewModel.cars.subscribe(onNext: { [weak self] list in
            guard let `self` = self else { return }
            list?.forEach {
                let annotation = WunderPointAnnotation()
                annotation.title = ($0.title ?? "").isEmpty ? "Wunder Car \($0.carID)" : $0.title
                annotation.coordinate = CLLocationCoordinate2D.init(latitude: $0.latitude ?? 0, longitude: $0.longitude ?? 0)
                annotation.identifier = $0.carID
                self.mapView.addAnnotation(annotation)
            }
            self.mapView.showAnnotations(self.mapView.annotations, animated: true)
        }).disposed(by: disposeBag)
    }
    
    private func navigate(withID: Int) {
        self.navigationController?.pushViewController(
            CarDetailsVC.init(
                carID: withID,
                viewModel: CarDetailsVM.init(carID: withID)),
            animated: true)
    }
}

extension CarClusterVC: CLLocationManagerDelegate, MKMapViewDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        mapView.mapType = MKMapType.standard
        userAnnotation.coordinate = locValue
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: locValue, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard
            let annotation = view.annotation,
            let wunderPoint = view.annotation as? WunderPointAnnotation else { return }
        mapView.annotations.filter { $0.title != annotation.title }.forEach {
            if $0.title != userAnnotation.title {
                self.mapView.view(for: $0)?.isHidden = true
            }
        }
        if wunderPoint.didTap {
            navigate(withID: wunderPoint.identifier ?? -1)
        } else {
            wunderPoint.didTap = true
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let view = MKAnnotationView(annotation: annotation, reuseIdentifier: "annotationView")
        view.isEnabled = true
        view.canShowCallout = true

        guard annotation.title != "Wunder User" else {
            view.image = R.image.usericon()
            return view
        }
        view.image = R.image.carImage()
        return view
    }
}
