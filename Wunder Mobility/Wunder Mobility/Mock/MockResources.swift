//
//  MockResources.swift
//  Wunder Mobility
//
//  Created by Mansi Shah on 8/31/19.
//  Copyright © 2019 Mansi Shah. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct APIRepository {
        
    func getCars(completion: @escaping ([Car]?, Error?) -> Void) {
        guard URL(string: "https://s3.eu-central-1.amazonaws.com/wunderfleet-recruiting-dev/cars.json") != nil
            else { fatalError() }
        completion(getCars(), nil)
    }
    
    func getCarDetails(completion: @escaping (CarDetails?, Error?) -> Void) {
        guard URL(string: "https://s3.eu-central-1.amazonaws.com/wunderfleet-recruiting-dev/cars/1") != nil
            else { fatalError() }
        completion(getCarDetails(), nil)
    }
    
    func rentCar(completion: @escaping (Bool?, Error?) -> Void) {
        guard URL(string: "https://s3.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-mobile-dev-quick-rental") != nil
            else { fatalError() }
        completion(true, nil)
    }
    
    private func getCars() -> [Car]? {
        let jsonObject = [
            [
                "carId": 1,
                "title": "Manfred",
                "lat": 51.5156,
                "lon": 7.4647,
                "licencePlate": "FBL 081",
                "fuelLevel": 27,
                "vehicleStateId": 0,
                "vehicleTypeId": 1,
                "pricingTime": "0,22/km",
                "pricingParking": "0,05/min",
                "reservationState": 0,
                "isClean": false,
                "isDamaged": true,
                "distance": "",
                "address": "Bissenkamp 4",
                "zipCode": "44135",
                "city": "Dortmund",
                "locationId": 2
            ],
            [
                "carId": 2,
                "title": "Anton",
                "lat": 51.511998333333,
                "lon": 7.4625316666667,
                "licencePlate": "162 NBT",
                "fuelLevel": 46,
                "vehicleStateId": 0,
                "vehicleTypeId": 2,
                "pricingTime": "",
                "pricingParking": "",
                "reservationState": 0,
                "isClean": true,
                "isDamaged": false,
                "distance": "",
                "address": "Kuhstraße 13",
                "zipCode": "44137",
                "city": "Dortmund",
                "locationId": 2
            ]
        ]
        
        guard let cars = try? JSONDecoder().decode([Car].self, from: JSON(jsonObject).rawData()) else { return nil }
        return cars
    }
    
    
    private func getCarDetails() -> CarDetails? {
        let jsonObject = [
            "carId": 1,
            "title": "Manfred",
            "isClean": false,
            "isDamaged": true,
            "licencePlate": "FBL 081",
            "fuelLevel": 27,
            "vehicleStateId": 0,
            "hardwareId": "HARDWARE",
            "vehicleTypeId": 1,
            "pricingTime": "0,22/km",
            "pricingParking": "0,05/min",
            "isActivatedByHardware": false,
            "locationId": 2,
            "address": "Bissenkamp 4",
            "zipCode": "44135",
            "city": "Dortmund",
            "lat": 51.5156,
            "lon": 7.4647,
            "reservationState": 0,
            "damageDescription": "Marius hat gesagt das muss so, und so, und so\r\nUnd wenn ich das verändere?",
            "vehicleTypeImageUrl": "https://wunderfleet-recruiting-dev.s3.eu-central-1.amazonaws.com/images/vehicle_type_image.png"
            ] as [String : Any]
        
        guard let carDetails = try? JSONDecoder().decode(CarDetails.self, from: JSON(jsonObject).rawData()) else { return nil }
        return carDetails
    }
}
