//
//  WunderPointAnnotation.swift
//  Wunder Mobility
//
//  Created by Mansi Shah on 9/2/19.
//  Copyright © 2019 Mansi Shah. All rights reserved.
//

import MapKit

class WunderPointAnnotation: MKPointAnnotation {
    var identifier: Int?
    var didTap: Bool = false
}
