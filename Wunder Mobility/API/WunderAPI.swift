//
//  WunderAPI.swift
//  Wunder Mobility
//
//  Created by Mansi Shah on 8/31/19.
//  Copyright © 2019 Mansi Shah. All rights reserved.
//

import Foundation

import SwiftyJSON
import RxSwift
import RxCocoa
import Alamofire

protocol WunderAPIProtocol {
    static func fetchCars() -> Observable<JSON>
    static func fetchDetails(of car: Int) -> Observable<JSON>
    static func rent(on car: Int) -> Observable<JSON>
}

struct WunderAPI: WunderAPIProtocol {
    
    // MARK: - API Addresses
    fileprivate enum Address: String {
        case cars = "wunderfleet-recruiting-dev/cars.json"
        case carDetails = "wunderfleet-recruiting-dev/cars/{carID}"
        case rent = "default/wunderfleet-recruiting-mobile-dev-quick-rental"
        
        private var baseURL: String { return "https://s3.eu-central-1.amazonaws.com/" }
        var addressString: String {
            return baseURL.appending(rawValue)
        }
    }
    
    // MARK: - API errors
    enum Errors: Error {
        case requestFailed
        case httpError(Int)
    }
    
    // MARK: - API Endpoint Requests
    static func fetchCars() -> Observable<JSON> {
        return request(
            addressString: Address.cars.addressString,
            method: .get)
    }
    
    static func fetchDetails(of car: Int) -> Observable<JSON> {
        return request(
            addressString: Address.carDetails.addressString.replacingOccurrences(of: "{carID}", with: "\(car)"),
            method: .get)
    }
    
    static func rent(on car: Int) -> Observable<JSON> {
        return request(
            addressString: Address.rent.addressString,
            method: .post,
            parameters: [ "carId" : car ],
            header: [ "Authorization" : "Bearer df7c313b47b7ef87c64c0f5f5cebd6086bbb0fa"])
    }
    
    // MARK: - Generic request to send an Alamofire Request
    static private func request<T: Any>(addressString: String, method: HTTPMethod, parameters: Parameters = [:], header: HTTPHeaders = [:]) -> Observable<T> {
        return Observable.create { observer in

            let request = Alamofire.request(addressString, method: method, parameters: parameters, encoding: URLEncoding.methodDependent, headers: header)
            request.validate(statusCode: 200 ..< 300)
            request.responseJSON { response in
                
                guard response.result.isSuccess else {
                    return observer.onError(Errors.httpError(response.response?.statusCode ?? -1))
                }
                
                guard
                    response.error == nil,
                    let data = response.data,
                    let json = JSON.init(rawValue: data) as? T else {
                        return observer.onError(Errors.requestFailed)
                }
                observer.onNext(json)
                observer.onCompleted()
            }
            
            return Disposables.create {
                request.cancel()
            }
        }
    }
}
