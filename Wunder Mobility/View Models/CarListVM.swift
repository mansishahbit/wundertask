//
//  CarListVM.swift
//  Wunder Mobility
//
//  Created by Mansi Shah on 9/2/19.
//  Copyright © 2019 Mansi Shah. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON

class CarListVM {
    
    // MARK: Properties
    private final let bag = DisposeBag()
    private final let apiType: WunderAPIProtocol.Type
    
    // MARK: Output
    let cars = BehaviorRelay<[Car]?>(value: nil)
    
    // MARK: Init
    init(apiType: WunderAPIProtocol.Type = WunderAPI.self) {
        self.apiType = apiType
        bindOutput()
    }
    
    // MARK: Workers
    final func bindOutput() {
        Observable.from(optional: fetchCars())
            .bind(to: cars)
            .disposed(by: bag)
    }
    
    /// Fetches list of cars
    ///
    /// - Returns: Array of car objects
    private func fetchCars() -> [Car]? {
        apiType.fetchCars().subscribe(
            onNext: { [weak self] json in
                do {
                    self?.cars.accept(try JSONDecoder().decode([Car].self, from: json.rawData()))
                } catch {
                    self?.cars.accept(nil)
                }
            },
            onError: { [weak self] error in
                self?.cars.accept(nil)
        }).disposed(by: bag)
        
        return nil
    }
}
