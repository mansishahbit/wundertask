//
//  CarDetailsVM.swift
//  Wunder Mobility
//
//  Created by Mansi Shah on 9/1/19.
//  Copyright © 2019 Mansi Shah. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON

class CarDetailsVM {
    
    // MARK: Properties
    private final let bag = DisposeBag()
    private final let apiType: WunderAPIProtocol.Type
    private final let carID: Int
    
    // MARK: Output
    let carDetails = BehaviorRelay<CarDetails?>(value: nil)
    let rentalResult = BehaviorRelay<Bool?>(value: nil)
    
    // MARK: Init
    init(apiType: WunderAPIProtocol.Type = WunderAPI.self, carID: Int) {
        self.carID = carID
        self.apiType = apiType
        bindOutput()
    }
    
    // MARK: Workers
    final func bindOutput() {
        Observable.from(optional: fetchCarDetails())
        .bind(to: carDetails)
        .disposed(by: bag)
    }
    
    /// Fetches car details for the current car
    ///
    /// - Returns: Details of the car as a CarDetails Object
    private func fetchCarDetails() -> CarDetails? {
        apiType.fetchDetails(of: carID).subscribe(
            onNext: { [weak self] json in
                do {
                   self?.carDetails.accept(try JSONDecoder().decode(CarDetails.self, from: json.rawData()))
                } catch {
                    self?.carDetails.accept(nil)
                }
        },
                onError: { [weak self] error in
                    self?.carDetails.accept(nil)
            }).disposed(by: bag)
        
        return nil
    }
    
    /// Perform rent action
    final func rentCar() {
        apiType.rent(on: carID).subscribe(
            onNext: { [weak self] json in
                guard json["reservationId"].int != nil else { return }
                self?.rentalResult.accept(true)
        },
            onError: { [weak self] error in
               self?.rentalResult.accept(false)
        }).disposed(by: bag)
    }
    
}
