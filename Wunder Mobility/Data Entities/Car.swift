//
//  Car.swift
//  Wunder Mobility
//
//  Created by Mansi Shah on 8/31/19.
//  Copyright © 2019 Mansi Shah. All rights reserved.
//

import Foundation

struct Car: Decodable {

    let carID: Int
    let title: String?
    let latitude: Double?
    let longitude: Double?
    
    enum CodingKeys: String, CodingKey {
        case carID = "carId"
        case title
        case latitude = "lat"
        case longitude = "lon"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        carID = try values.decode(Int.self, forKey: .carID)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        latitude = try values.decodeIfPresent(Double.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(Double.self, forKey: .longitude)
    }
}
