//
//  CarDetails.swift
//  Wunder Mobility
//
//  Created by Mansi Shah on 8/31/19.
//  Copyright © 2019 Mansi Shah. All rights reserved.
//

import Foundation

struct CarDetails: Decodable {
    
    let carID: Int
    let title: String
    let isClean: Bool
    let isDamaged: Bool
    let license: String
    let fuel: Int
    let pricingTime: String
    let pricingParking: String
    let address: String
    let zip: String
    let city: String
    let damageDesc: String
    let imageURLString: String
    
    enum CodingKeys: String, CodingKey {
        case carID = "carId"
        case title
        case isClean
        case isDamaged
        case license = "licencePlate"
        case fuel = "fuelLevel"
        case pricingTime
        case pricingParking
        case address
        case zip = "zipCode"
        case city
        case damageDesc = "damageDescription"
        case imageURLString = "vehicleTypeImageUrl"
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        carID = try values.decode(Int.self, forKey: .carID)
        title = try values.decode(String.self, forKey: .title)
        isClean = try values.decode(Bool.self, forKey: .isClean)
        isDamaged = try values.decode(Bool.self, forKey: .isDamaged)
        license = try values.decode(String.self, forKey: .license)
        fuel = try values.decode(Int.self, forKey: .fuel)
        pricingTime = try values.decode(String.self, forKey: .pricingTime)
        pricingParking = try values.decode(String.self, forKey: .pricingParking)
        address = try values.decode(String.self, forKey: .address)
        zip = try values.decode(String.self, forKey: .zip)
        city = try values.decode(String.self, forKey: .city)
        damageDesc = try values.decode(String.self, forKey: .damageDesc)
        imageURLString = try values.decode(String.self, forKey: .imageURLString)
    }
}
